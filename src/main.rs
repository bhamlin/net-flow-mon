// #![allow(unused)]
mod cli_args;
mod net;

use chrono::Utc;

fn main() {
    let args = cli_args::parse_args();

    let device_result = net::get_interface(&args.if_name);
    match device_result {
        None => {
            if !args.if_name.is_empty() {
                println!("No interface '{}' found", args.if_name);
            } else {
                println!("No device available");
            }
        }
        Some(device) => {
            let mut cap = net::capture_device(device).expect("Unable to begin capture");

            while let Ok(packet) = cap.next_packet() {
                match net::parse_packet(&packet) {
                    Some(p) => {
                        let utc = Utc::now();
                        println!("{}.{},{}", utc.timestamp(), utc.timestamp_subsec_nanos(), p);
                    }
                    _ => (),
                }
            }
        }
    }
}
