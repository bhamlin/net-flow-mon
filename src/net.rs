pub mod layer2;
pub mod layer3;
pub mod layer4;
mod types;

use pcap::{Active, Capture, Device, Packet};

use self::{
    layer2::get_l2_type,
    layer3::{get_l3_flow, get_l3_type},
    layer4::get_datagram_port_flow,
    types::{AddressV4, NetPacket},
};

const L2_IPV4: u16 = 0x0800;
const L3_TCP: u8 = 0x06;
const L3_UDP: u8 = 0x11;

pub fn get_interface(if_name: &String) -> Option<Device> {
    if !if_name.is_empty() {
        let devices = Device::list().expect("Device lookup failed");
        for intf in devices {
            if intf.name.eq(if_name) {
                return Some(intf);
            }
        }

        None
    } else {
        Device::lookup().expect("Device lookup failed")
    }
}

pub fn capture_device(device: Device) -> Option<Capture<Active>> {
    match Capture::from_device(device) {
        Ok(dev) => {
            let capture = dev.promisc(true).snaplen(64).immediate_mode(true);

            match capture.open() {
                Ok(capture_ready) => Some(capture_ready),
                _ => None,
            }
        }
        _ => None,
    }
}

pub fn parse_packet(packet: &Packet) -> Option<NetPacket> {
    let mut output = NetPacket::default();

    let l2_type = get_l2_type(packet);
    output.l2_type = l2_type;
    if l2_type == L2_IPV4 {
        let l3_type = get_l3_type(packet);
        let l3_data = get_l3_flow(packet);

        output.l3_type = l3_type;
        output.length = l3_data.length;

        output.source.host = AddressV4::from_octets(l3_data.source);
        output.target.host = AddressV4::from_octets(l3_data.target);

        if (l3_type == L3_TCP) || (l3_type == L3_UDP) {
            let l4_data = get_datagram_port_flow(packet);
            output.source.port = l4_data.source;
            output.target.port = l4_data.target;
        }

        Some(output)
    } else {
        None
    }
}
