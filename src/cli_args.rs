use clap::Parser;

#[derive(Parser, Debug)]
#[clap(author, version, about, long_about=None)]
pub struct Args {
    #[clap(short = 'v', long = "verbose", action)]
    pub verbose: bool,

    #[clap(short = 'i', long = "interface", default_value = "")]
    pub if_name: String,
}

pub fn parse_args() -> Args {
    Args::parse()
}
