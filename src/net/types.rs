use std::fmt;
use std::fmt::Display;
use string_join::Join;

pub struct AddressV4 {
    pub octets: [u8; 4],
}

pub struct NetEndpoint {
    pub host: AddressV4,
    pub port: u16,
}

pub struct NetPacket {
    pub length: u16,
    pub l2_type: u16,
    pub l3_type: u8,
    pub source: NetEndpoint,
    pub target: NetEndpoint,
}

impl AddressV4 {
    pub fn from_octets(octets: [u8; 4]) -> AddressV4 {
        AddressV4 { octets }
    }
}

impl Default for NetEndpoint {
    fn default() -> Self {
        NetEndpoint {
            host: AddressV4 { octets: [0; 4] },
            port: 0,
        }
    }
}

impl Display for AddressV4 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            ".".join(self.octets.into_iter().map(|d| d.to_string()))
        )
    }
}

impl Display for NetEndpoint {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:}:{}", self.host, self.port)
    }
}

impl Default for NetPacket {
    fn default() -> Self {
        NetPacket {
            length: 0,
            l2_type: 0,
            l3_type: 0,
            source: NetEndpoint::default(),
            target: NetEndpoint::default(),
        }
    }
}

impl Display for NetPacket {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{},{},{}", self.source, self.target, self.length)
    }
}
