use byteorder::{BigEndian, ByteOrder};
use pcap::Packet;

type Address = [u8; 4];

pub struct Layer3Flow {
    pub source: Address,
    pub target: Address,
    pub length: u16,
}

pub fn get_l3_flow(packet: &Packet) -> Layer3Flow {
    let source: Address = packet.data[26..30].try_into().unwrap();
    let target: Address = packet.data[30..34].try_into().unwrap();
    let length: u16 = BigEndian::read_u16(&packet.data[16..18]);

    Layer3Flow {
        source,
        target,
        length,
    }
}

pub fn get_l3_type(packet: &Packet) -> u8 {
    packet.data[23]
}
