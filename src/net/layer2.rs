use byteorder::{BigEndian, ByteOrder};
use pcap::Packet;

pub fn get_l2_type(packet: &Packet) -> u16 {
    return BigEndian::read_u16(&packet[12..14]);
}
