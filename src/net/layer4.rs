use byteorder::{BigEndian, ByteOrder};
use pcap::Packet;

type Port = u16;

pub struct Layer4Flow {
    pub source: Port,
    pub target: Port,
}

pub fn get_datagram_port_flow(packet: &Packet) -> Layer4Flow {
    let source = BigEndian::read_u16(&packet.data[34..36]);
    let target = BigEndian::read_u16(&packet.data[36..38]);

    Layer4Flow {
        source: source,
        target: target,
    }
}

#[allow(unused)]
pub fn get_tcp_port_flow(packet: &Packet) -> Layer4Flow {
    get_datagram_port_flow(packet)
}

#[allow(unused)]
pub fn get_udp_port_flow(packet: &Packet) -> Layer4Flow {
    get_datagram_port_flow(packet)
}
